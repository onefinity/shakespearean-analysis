var _ = require('lodash'),
    fs = require('fs'),
    path = require('path'),
    async = require('async'),
    extend = require('extend'),
    slugify = require('slug'),
    urljoin = require('url-join'),
    request = require('request'),
    cheerio = require('cheerio'),
    readingTime = require('reading-time'),
    wtf_wikipedia = require('wtf_wikipedia');

// Add lodash mixins
_.mixin(require('lodash-inflection'));
_.mixin({ // Crushes multidimensional object into a one-dimensional array
    crush: function(list, shallow, r) {
        return _.isObject(list) ? (r = function(list) {  // It doesn't matter that "r" might have
            return _.isObject(list) ? _.flatten(_.map(list, shallow ? _.identity : r)) : list;
        })(list) : [];
    }
});

// Define source
var source = 'http://shakespeare.mit.edu/';

// Scrape source
request(source, function(err, response, html) {
    if ( err ) throw err;
    if ( response.statusCode !== 200 ) throw 'Status was not OK';

    var $ = cheerio.load(html),
        $table = $('body > p > table[cellpadding="5"]');

    var skipCategories = ['Poetry'],
        plays = {},
        categories = [];

    // Parse categories
    console.log('Scraping categories...');
    $table.find('tr:first-child td').each(function(i, el) {
        categories[i] = $(el).text().trim();
    });
    console.log('Found %s categories', _.keys(categories).length);

    // Parse plays
    console.log('Scraping plays...');
    $table.find('a').each(function(i, el) {
        var $el      = $(el),
            $parent  = $el.closest('td'),
            title    = $el.text().trim().replace('\n', ' '),
            slug     = slugify(title.toLowerCase()),
            category = categories[$parent.index()],
            play_url = urljoin(source, $el.attr('href'));

        if ( _.contains(skipCategories, category) ) return;

        plays[slug] = {
            title:    title,
            slug:     slug,
            category: category,
            index:    play_url,
            full:     play_url.replace(/[^/]*$/, 'full.html')
        };
    });

    console.log('Found %s plays, let\'s scrape \'em!', _.keys(plays).length);

    // Now let's parse each play's full page
    var scrapers = {};
    _.each(plays, function(play, id) {
        scrapers[id] = function(cb) {
            request(play.full, function(err, response, html) {
                console.log('Scraping \'%s\' from %s...', play.title, play.full);
                cb(null, html);
            });
        };

        //return false; // Comment this to only scrape/parse a single play for debugging purposes
    });

    async.parallel(scrapers, function(err, results) {
        if ( err ) throw err;

        var parsers = {};
        _.each(results, function(result, id) {
            // Scrape MIT
            parsers[id + ':mit'] = function(callback) {
                var play = {};
                var $ = cheerio.load(result),
                    $body = $('body');

                $body.find('table').remove();
                $body.children().each(function(i, el) {
                    var $el = $(el),
                        type = $el.get(0).tagName;

                    switch ( type.toLowerCase() ) {
                        case 'h3':
                            var text = $el.text();

                            if ( text.substring(0, 3).toLowerCase() === 'act' ) {
                                if ( typeof play.acts === 'undefined' ) play.acts = [];
                                play.acts.push({ title: text });
                            } else {
                                // Make sure to add an empty 'act' when a play doesn't start with one (e.g. The Taming of the Shrew)
                                if ( typeof play.acts === 'undefined' ) play.acts = [{}];

                                // Add the scene
                                var act = play.acts.length - 1;
                                if ( typeof play.acts[act].scenes === 'undefined' ) play.acts[act].scenes = [];
                                play.acts[act].scenes.push({ title: text });
                            }

                            break;
                        case 'p':
                            $el.find('blockquote').each(function(i, el) {
                                // Make sure to add an empty 'scene' when an act doesn't start with one (e.g. Perticles - ACT II)
                                var act = play.acts.length - 1;
                                if ( typeof play.acts[act].scenes === 'undefined' ) play.acts[act].scenes = [{}];

                                var $el = $(el),
                                    scene = play.acts[act].scenes.length - 1;
                                if ( typeof play.acts[act].scenes[scene].content === 'undefined' ) play.acts[act].scenes[scene].content = [];

                                var character,
                                    cache = [];
                                $el.children().each(function(i, el) {
                                    var $child = $(el),
                                        type = $child.get(0).tagName;

                                    switch ( type ) {
                                        case 'a':
                                            cache.push($child.text().trim());
                                            character = $el.prev().text().trim();
                                            break;
                                        case 'i':
                                        case 'p':
                                            cache.push('<<' + $child.text().trim() + '>>')
                                            break;
                                    }
                                });

                                if ( cache.length ) {
                                    var result = {};
                                    if ( typeof character !== 'undefined' ) result.character = character;
                                    result.text = cache;

                                    play.acts[act].scenes[scene].content.push(result);
                                }
                            });

                            break;
                    }
                });

                console.log('Scraped MIT for \'%s\'...', plays[id].title);
                callback(null, extend(true, {}, plays[id], play));
            };

            // Scrape Wikipedia
            parsers[id + ':wiki'] = function(callback) {
                wtf_wikipedia.from_api(plays[id].title + '_(play)', 'en', function(markup) {
                    async.parallel([
                        function(callback) {
                            if ( !markup ) {
                                wtf_wikipedia.from_api(plays[id].title, 'en', function(markup) {
                                    if ( markup.substring(0, 9).toLowerCase() === '#redirect' ) {
                                        var title = markup.match(/\[\[([^)]+)]]/)[1];
                                        wtf_wikipedia.from_api(title, 'en', function(markup) {
                                            callback(null, {
                                                article: title.replace(/_/g, ' '),
                                                plaintext: wtf_wikipedia.plaintext(markup)
                                            });
                                        });
                                    } else {
                                        callback(null, {
                                            article: plays[id].title.replace(/_/g, ' '),
                                            plaintext: wtf_wikipedia.plaintext(markup)
                                        });
                                    }
                                });
                            } else {
                                // Handle redirects for incorrect titles
                                if ( markup.substring(0, 9).toLowerCase() === '#redirect' ) {
                                    var title = markup.match(/\[\[([^)]+)]]/)[1];
                                    wtf_wikipedia.from_api(title, 'en', function(markup) {
                                        callback(null, {
                                            article: title.replace(/_/g, ' '),
                                            plaintext: wtf_wikipedia.plaintext(markup)
                                        });
                                    });
                                } else {
                                    callback(null, {
                                        article: (plays[id].title + '_(play)').replace(/_/g, ' '),
                                        plaintext: wtf_wikipedia.plaintext(markup)
                                    });
                                }
                            }
                        }
                    ], function(err, results) {
                        if ( err ) throw err;
                        console.log('Scraped Wikipedia for \'%s\'', plays[id].title);
                        callback(null, { wiki: results[0] });
                    });
                });
            };

            // Scrape openlibrary.org
            parsers[id + ':ol'] = function(callback) {
                request('https://openlibrary.org/search.json?title=' + plays[id].title, function(err, response, json) {
                    // Filter out all results not written by Shakespeare
                    var entries = _.filter(JSON.parse(json).docs, function(entry) {
                        return _.contains(entry.author_name, 'William Shakespeare');
                    });

                    // Find the one that has the oldest publish date
                    var original = {};
                    _.each(entries, function(entry) {
                        if ( typeof original === 'undefined' ) original = entry;
                        if ( typeof original.first_publish_year === 'undefined' ) original = entry
                        if ( typeof entry.first_publish_year !== 'undefined' && entry.first_publish_year < original.first_publish_year ) original = entry;
                    });

                    console.log('Scraped Open Library for \'%s\'', plays[id].title);
                    callback(null, {
                        published: original.first_publish_year,
                        author: 'William Shakespeare'
                    });
                });
            };
        });

        console.log('Parsing data...');
        async.parallel(parsers, function(err, results) {
            if ( err ) throw err;

            var data = {};
            _.each(results, function(result, key) {
                var play = key.split(':')[0];
                if ( typeof data[play] === 'undefined' ) data[play] = {};

                // Extend
                extend(true, data[play], result, {
                    reading_time: readingTime(_.crush(data[play].acts).join('\n')).text
                });
            });

            _.each(data, function(play, id) {
                console.log('Normalizing characters for \'%s\'', play.title);
                var characters = [],
                    contents = [];

                _.each(play.acts, function(act) {
                    _.each(act.scenes, function(scene) {
                        _.each(scene.content, function(content) {
                            if ( typeof content.character === 'undefined' ) return;

                            var character = content.character.toLowerCase();
                            if ( !_.contains(characters, character) ) characters.push(character);

                            // Save content, too
                            contents.push(content.text.join('\n'));
                        });
                    });
                });

                characters = _.map(characters, function(character) {
                    var regexp = new RegExp(character, 'gi'),
                        match,
                        matches = [];

                    // Find all occurrences of the name in both the Wiki article and the entire play
                    while ( (match = regexp.exec(play.wiki.plaintext + contents.join('\n'))) !== null ) {
                        var m = match[0];

                        // Filter out if this name is all uppercase
                        if ( m === m.toUpperCase() ) continue;

                        // Filter out if this name doesn't start with an uppercase letter
                        var first = m.charAt(0);
                        if ( first === first.toLowerCase() ) continue;

                        matches.push(m);
                    }

                    // Get the value that occurres the most in our array (http://stackoverflow.com/a/18878650/1358948)
                    // or use lodash-inflection's titleize if there were no matches for the name
                    return _.chain(matches).countBy().pairs().max(_.last).head().value() || _.titleize(character);
                });

                // Go through the entire play again and fix character names
                _.each(characters, function(character) {
                    var regex = new RegExp(character.toUpperCase(), 'g');

                    _.each(play.acts, function(act) {
                        // Let's also fix ACT and SCENE while were at it
                        if ( typeof act.title !== 'undefined' ) {
                            act.title = act.title.replace(/ACT/g , 'Act');
                            act.title = act.title.replace(regex, character);
                        }

                        _.each(act.scenes, function(scene) {
                            if ( typeof scene.title !== 'undefined' ) {
                                scene.title = scene.title.replace(/SCENE/g , 'Scene');
                                scene.title = scene.title.replace(regex, character);
                            }

                            _.each(scene.content, function(content) {
                                // Fix character
                                if ( typeof content.character !== 'undefined' ) content.character = content.character.replace(regex, character);

                                // Fix characters in text
                                content.text = _.map(content.text, function(line) {
                                    return line.replace(regex, character);
                                });
                            });
                        });
                    });
                });

                // Add characters to data
                extend(true, play, {
                    characters: _.filter(characters, function(character) {
                        return !_.contains(['all', 'both', 'some'], character.toLowerCase());
                    })
                });

                fs.writeFile(path.join(__dirname, 'output/', id + '.json'), JSON.stringify(play, null, 4), function(err) {
                    if ( err ) throw err;
                    console.log('Saved JSON to \'data/output/%s.json\'', id);
                });
            });

            // Create a plays.json file containing basic information about each available play
            var plays = {
                plays: _.sortBy(_.map(data, function(play) {
                    return {
                        id: play.slug,
                        title: play.title,
                        author: play.author,
                        category: play.category,
                        published: play.published,
                        reading_time: play.reading_time
                    };
                }), 'title')
            }
            fs.writeFile(path.join(__dirname, 'plays.json'), JSON.stringify(plays, null, 4), function(err) {
                if ( err ) throw err;
                console.log('Saved JSON to \'data/plays.json\'');
            });
        });
    });
});