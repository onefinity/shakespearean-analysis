var $ = require('jquery'),
    _ = require('lodash'),
    Vue = require('vue'),
    extend = require('extend');

$(function() {
    $.getJSON('data/plays.json', function(data) {
        // List plays
        new Vue({
            el: '#plays',
            data: data
        });

        // Debugging: just select the first play and roll with it
        //$('.plays input:radio[name="play"]:first').trigger('click');
        //$('.plays-analyze .button').trigger('click');
    });

    $('.plays').on('change', 'input:radio[name="play"]', function() {
        $('.plays-analyze').toggleClass('is-visible', !!$(this).val());
    });
    $('.plays-analyze .button').on('click', function(e) {
        e.preventDefault();

        var play = $('.plays input:radio[name="play"]:checked').val();
        $.getJSON('data/output/' + play + '.json', function(data) {
            new Vue({
                el: '#play',
                data: data
            });

            $('.plays').removeClass('is-visible');
            $('.play').addClass('is-visible');
        });
    });

    $('.back-to-plays').on('click', function(e) {
        e.preventDefault();

        $('.plays').addClass('is-visible');
        $('.play').removeClass('is-visible');
    });
    $('.play').on('click mouseup keyup', function() {
        setTimeout(function() {
            var $el = $('.play-options'),
                sel = window.getSelection().toString().trim();
            var available_classes = {
                word: 'play-options--word',
                words: 'play-options--words',
                sentence: 'play-options--sentence',
                sentences: 'play-options--sentences',
                paragraphs: 'play-options--paragraphs',
            };

            if ( sel !== '' ) {
                var count = {
                    characters: sel.replace(/\s/g, '').length,
                    words: sel.split(/\s+/).length,
                    sentences: sel.split(/\.|\?|!/).length - 1
                };

                // Add main visibility class
                $el.removeClass(_.values(available_classes).join(' ')).addClass('is-visible');

                // Add defining classes
                var classes = [];
                if ( count.words === 1 ) classes.push(available_classes.word);
                if ( count.words > 1 && count.sentences <= 0 ) classes.push(available_classes.words);
                if ( count.sentences === 1 ) classes.push(available_classes.sentence);
                if ( count.sentences > 1 ) classes.push(available_classes.sentences);
                if ( count.sentences > 5 ) classes.push(available_classes.paragraphs);
                $el.addClass(classes.join(' '));
            } else {
                // Hide UI
                $el.removeClass('is-visible');
            }
        }, 0);
    });
});